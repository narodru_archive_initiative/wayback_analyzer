# -*- coding: utf-8 -*-
import requests
import os
import sys
from bs4 import BeautifulSoup
import csv
from itertools import chain
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


session = requests.Session()
retry = Retry(connect=3, backoff_factor=0.5)
adapter = HTTPAdapter(max_retries=retry)
session.mount('http://', adapter)
session.mount('https://', adapter)
# use session.get instead of requests.get

def getpage(url):
    info = dict()
    
    if (url[0:4] != 'http'):
        url = r'http://' + url
    
    info['site'] = url
    
    response = session.get(url)
    
    info['code'] = str(response.status_code)
    if info['code'] == '410':
        info['code'] = '410 - Site removed by Ucoz'
    elif info['code'] == '404':
        info['code'] = '410 - Site removed by Yandex'
    
    available = session.get('http://archive.org/wayback/available?url=' + url).json()["archived_snapshots"]
    
    if available is None:
        info["available"] = False
    else:
        info["available"] = True
        
        first_url = 'https://web.archive.org/web/1000/' + url
        info['first_url'] = first_url
        first = session.get(first_url)
        info['first_date'] = first.headers['Memento-Datetime']
            
        latest_url = 'https://web.archive.org/web/' + url
        info['latest_url'] = latest_url
        latest = session.get(latest_url)
        info['latest_date'] = latest.headers['Memento-Datetime']
        
        info['wayback'] = 'http://web.archive.org/web/%2A/' + url
    
        info['captured'] = str(get_snaps_num(url))
    

    if info['code'][0] == '4':
        available_url = find_available(url)
        info['last-avail'] = available_url
    
    title = get_title(available_url)
    info['title'] = title
    
    with open(os.path.join('.', 'db', 'info_csv.csv'), "a", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=columns)
        try:
            writer.writerow(info)
        except UnicodeEncodeError:
            pass
    
    return info


def find_available(url):
    
    data = requests.get('http://web.archive.org/__wb/sparkline?url=' + url 
                            + '&collection=web&output=json').json()
    f_year = int(data['first_ts'][0:4])
    l_year = int(data['last_ts'][0:4])
    cur_year = l_year-1
    code = '404'
    while cur_year != f_year and code[0] == '4':
        av_url = 'http://web.archive.org/web/' + str(cur_year) + '/' + url
        try:
            code = str(requests.get(av_url).status_code)
        except:
            pass
        cur_year -= 1
    next_year = cur_year+2
    next_y_url = 'http://web.archive.org/web/' + str(next_year) + '01' + '/' + url
    print(next_y_url)
    try:
        next_y_status = requests.get(next_y_url).status_code
    except:
        return av_url
    if str(next_y_status)[0] != '4':
        month = 12
        month_s = '12'
        while month != 0:
            av_url = 'http://web.archive.org/web/' + str(next_year) + month_s + '/' + url
            print(month, av_url)
            try:
                code = str(requests.get(av_url).status_code)
            except:
                return av_url
            if code[0] != '4':
                print(av_url)
                return av_url
            month -= 1
            month_s = str(month) if len(str(month)) == 2 else '0' + str(month)
    return av_url
    

def get_title(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    title = soup.find('title')
    if title is not None:
        title = title.string.strip().replace('\n', '').replace('\r', '')
    return title

def get_snaps_num(url):
    response = requests.get('http://web.archive.org/__wb/sparkline?url=' + url
                            + '&collection=web&output=json').json()
    nums = list(response['years'].values())
    nums = chain.from_iterable(nums)
    captured = sum(list(nums))
    return captured

def print_info(info):
    print('Site: ' + info['site'])
    print('Status code: ' + str(info['code']))
    print('Title: ' + info['title'])
    if (info["available"] == False):
        print("Site isn't archieved in Wayback Machine")
        print('\n')
    else:
        print('Wayback: ' + info['captured'] + ' (' + info['wayback'] + ')')
        print('First saved: ' + info['first_date'] + ' (' + info['first_url'] + ')')
        print('Last saved: ' + info['latest_date'] + ' (' + info['latest_url'] + ')')
        if info.get('last-avail') is not None:
            print("Last available snapshot: " + info['last-avail'])
        print('\n')

def get_post_text(info):
    post_text = ""
    post_text += ('**Site**: %s\n' % info['site'])
    post_text += ('**Title**: %s\n' % info['title'])
    
    if (info["available"] == False):
        post_text += "Site isn't archieved in Wayback Machine"
    else:
        post_text += ('**Wayback**: [%s shots](%s)\n' % (info['captured'], info['wayback']))
        post_text += ('**First saved**: [%s](%s)\n' % (info['first_date'][:-13], info['first_url']))
        post_text += ('**Last saved**: [%s](%s)\n' % (info['latest_date'][:-13], info['latest_url']))
    
    post_text += "#site"
    return post_text

def init_csv():
    with open(os.path.join('.', 'db', 'info_csv.csv'), "w", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=columns)
        writer.writeheader()

basepath = os.path.join('.', 'db')
urlpath = os.path.join('.', 'db', 'urls')
urls = open(urlpath)
columns = ['site', 'code', 'title', 'captured', 'wayback', 'first_date', 
                   'first_url', 'latest_date', 'latest_url', 'available', 'last-avail']

init_csv()

if len(sys.argv) == 1:
    urls = [x.rstrip() for x in open(urlpath) if x.rstrip() != ""]
    print("Checking following urls from db: " + ", ".join(urls))

    with open(urlpath) as urls:
        for url in urls:
            url = url.strip()
            if (url == ''): # skip empty
                continue
            else:
                info = getpage(url)
                print_info(info)

    n = input("\nPress enter to exit")
else:
    urls = sys.argv
    urls.pop(0)
    print("Checking following urls from command line: " + ", ".join(urls))
    for url in urls:
        info = getpage(url)
        print(get_post_text(info))
